"""
Autor: Francisco Andrade
Data: 13/01/2018
Descrição: Funções referentes à entrada de dados dos campos que serão persistidos no arquivo binario
"""
def codigo_input(records, menu):
    """
    Trata a entrada de dados no campo codigo
    """
    mensagem = "Codigo da peca (0 volta ao menu principal): "

    while True:
        codigo = input(mensagem)

        if not codigo:
            mensagem = "Codigo nao pode ser vazio. Informe outro (0 volta ao menu principal): "
        elif any(r.get("Codigo", None) == codigo for r in records):
            mensagem = "Codigo da peca ja existe. Informe outro (0 volta ao menu principal): "
        elif codigo == "0":
            menu()
        else:
            break

    return codigo

def quantidade_input():
    """
    Valida a entrada do campo Quantidade
    """
    quantidade = 0
    while True:
        try:
            quantidade = int(input("Quantidade: "))
            break
        except ValueError:
            print("Valor invalido para o campo quantidade. Deve ser numerico.")

    return quantidade

def preco_input():
    """
    Valida a entrada do campo Preço
    """
    preco = 0
    while True:
        try:
            preco = float(input("Preco: "))
            break
        except ValueError:
            print("Valor invalido para o campo preco. Deve ser numerico.")

    return preco
