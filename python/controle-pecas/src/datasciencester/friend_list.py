
"""
"""

USERS = [
    {"id": 0, "name": "Hero"},
    {"id": 1, "name": "Dunn"},
    {"id": 2, "name": "Sue"},
    {"id": 3, "name": "Chi"},
    {"id": 4, "name": "Thor"},
    {"id": 5, "name": "Clive"},
    {"id": 6, "name": "Hicks"},
    {"id": 7, "name": "Devin"},
    {"id": 8, "name": "Kate"},
    {"id": 9, "name": "Klein"} 
]

FRIENDSHIPS = [(0, 1), (0, 2), (1, 2), (1, 3), (2, 3), (3, 4),
               (4, 5), (5, 6), (5, 7), (6, 8), (7, 8), (8, 9)]


for user in USERS:
    user["friends"] = []

for i, j in FRIENDSHIPS:
    # this works because users[i] is the user whose id is i
    USERS[i]["friends"].append(USERS[j]) # add i as a friend of j
    USERS[j]["friends"].append(USERS[i]) # add j as a friend of i

def number_of_friends(user):
    """how many friends does _user_ have?"""
    return len(user["friends"]) # length of friend_ids list

if __name__ == "__main__":
    total_connections = sum(number_of_friends(user)
                            for user in USERS) # 24
    num_users = len(USERS) # length of the users list 
    avg_connections = total_connections / num_users   # 2.4 

    # create a list (user_id, number_of_friends) 
    num_friends_by_id = [(user["id"], number_of_friends(user))
                         for user in USERS]
    
    # get it sorted by num_friends (largest to smallest)
    
    print("Numero de conexoes: %s" %total_connections)
    print("Numero de conexoes media: %s" %avg_connections)
    print(sorted(num_friends_by_id, key=lambda num_friends: num_friends[1], reverse=True))

    # itemgetter is more readable in this case.
    # It is also faster since almost all of the computation will be done on the c side 
    # rather than through the use of lambda.abs
    # https://stackoverflow.com/questions/10695139/sort-a-list-of-tuples-by-2nd-item-integer-value
    from operator import itemgetter
    print(sorted(num_friends_by_id, key=itemgetter(1), reverse=True))
