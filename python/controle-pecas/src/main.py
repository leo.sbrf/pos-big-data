"""
Autor: Francisco Andrade
Data: 12/01/2018
Descrição: Essa aplicação permite que um usuário realize a manutenção do registro de peças
através da interface de linha de comando.
"""
import sys
import os
import pickle
from user_input import codigo_input, quantidade_input, preco_input

MAX_FILE_RECORDS = 50
RECORDS_PER_PAGE = 10

def check_file():
    """
    Verifica se o arquivo existe, se não existe cria a primeira vez
    """
    with open("data.bin", "ab") as file:
        print("Criando arquivo binario se ainda nao existir...")

def read_file():
    """
    Lê todos os registros do arquivo binário e retorna um array de registros
    """
    records = []
    with open("data.bin", "rb") as file:
        while True:
            try:
                records.append(pickle.load(file))
            except EOFError:
                break

    return records

def main_menu():
    """
    Menu principal da aplicação
    """
    clear_screen()
    while True:
        choice = 0
        print("Gerenciador de pecas. Selecione uma opcao")
        print("1 - Inserir")
        print("2 - Listar")
        print("3 - Consultar")
        print("4 - Remover")
        print("9 - Sair")
        print() # espaco
        try:
            choice = int(input("Opcao: "))
        except ValueError:
            print("Opcao invalida!\n")

        if choice == 1:
            insert_menu()
        elif choice == 2:
            list_menu(read_file(), main_menu)
        elif choice == 3:
            inspect_menu()
        elif choice == 4:
            remove()
        elif choice == 9:
            print("Saindo do sistema...")
            sys.exit()
        else:
            clear_screen()
            print("Opcao invalida!\n")

def inspect(key, value, records):
    """
    Inspeciona os registros e retorna aqueles cuja chave contem o valor passado como parâmetro
    """
    result = []

    for record in records:
        key_value = record.get(key, None)
        if isinstance(key_value, str) and value.lower() in key_value.lower(): # compara case insensitive
            result.append(record)
        elif key_value == value: # comparacao crua
            result.append(record)

    return result

def inspect_menu():
    """
    Lista os resultados de uma consulta.
    """
    clear_screen()
    records = read_file()
    while True:
        choice = 0
        print("Como deseja realizar a consulta:")
        print("1 - Por Codigo")
        print("2 - Por Peca")
        print("3 - Por Preco")
        print("9 - Voltar")
        print() # espaco
        try:
            choice = int(input("Opcao: "))
        except ValueError:
            print("Opcao invalida!\n")

        if choice == 1:
            codigo = input("Informe o codigo: ")
            list_menu(inspect("Codigo", codigo, records), None)
        elif choice == 2:
            peca = input("Informe a peca: ")
            list_menu(inspect("Peca", peca, records), None)
        elif choice == 3:
            preco = float(input("Informe o preco: "))
            list_menu(inspect("Preco", preco, records), None)
        elif choice == 9:
            main_menu()
        else:
            clear_screen()
            print("Opcao invalida!\n")

def list_menu(records, callback):
    """
    Lista todos os registros do arquivo.
    A regra para a formatação do texto é:
    %60s significa texto justificado à direita com 60 caracteres.
    %-10s significa texto justificado à esquerda com 10 caracteres.
    """
    clear_screen()
    records_counter = 0

    for record in records:
        records_counter += 1
        print("%-6s |%-30s |%-60s |%-4s |%-10s" % (record["Codigo"].upper(), record["Peca"].upper(), record["Descricao"].upper(), record["Quantidade"], record["Preco"]))
        if records_counter == RECORDS_PER_PAGE:
            input("Mais...")

    print("\n")
    input("Fim da listagem. Pressione qualuquer tecla para continuar...")

    if callback:
        callback()

def insert_menu():
    """
    Insere um novo registro no arquivo vinário, mas antes faz as validações necessárias:
    - Código não pode ser duplicado;
    - Quantidade máxima de peças é igual a 50;
    """
    clear_screen()
    records = read_file()

    # verifica a quantidade maxima de peças
    if len(records) == MAX_FILE_RECORDS:
        input("O registro esta cheio. Maximo permitido e %s. Para continuar remova pelo menos um "
              "registro. Aperte qualquer tecla para voltar ao menu principal..." %MAX_FILE_RECORDS)
        main_menu()


    codigo = codigo_input(records, main_menu)

    peca = input("Peca: ")
    descricao = input("Descricao: ")
    quantidade = quantidade_input()
    preco = preco_input()

    record = {"Codigo": codigo, "Peca": peca, "Descricao": descricao, "Quantidade": quantidade,
              "Preco": preco}

    with open("data.bin", "ab") as file:
        pickle.dump(record, file)

    input("Registro inserido com sucesso! Pressione qualquer tecla para continuar...")

    main_menu()

def remove():
    """
    Remove uma peca pelo codigo persistindo essa acao no arquivo binario
    """
    records = read_file()
    to_remove = inspect("Codigo", input("Informe o codigo: "), records)

    print(to_remove)
    confirmation = input("Tem certeza que deseja remover? (Tecle 'S' para confirmar): ")

    if confirmation.upper() == 'S':
        for record in to_remove:
            records.remove(record)

        with open("data.bin", "wb") as file:
            for record in records:
                pickle.dump(record, file)
        
        input("Operacao concluida! Pressione qualquer tecla para continuar...")
    else:
        input("Operacao cancelada! Pressione qualquer tecla para continuar...")

def clear_screen():
    """
    Limpa a tela. Para sistemas baseados no NT ou POSIX
    """
    os.system('cls' if os.name == 'nt' else 'clear')

if __name__ == "__main__":
    check_file()
    main_menu()
